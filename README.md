# postman新手使用攻略

|
    仅供参考：
    postman上手教程：请根据目录图片操作指引完成即可正常给前端访问接口
|
##  请先下载postman工具，自行搜索下载

### 1.新建服务
|
[<img src="./1.jpg" alt="IE / Edge" width="100%"/>](./1.jpg)
|
### 2.创建接口
|
[<img src="./2.jpg" alt="IE / Edge" width="100%" />](./2.jpg)
|
### 3.创建接口集合
|
[<img src="./3.jpg" alt="IE / Edge" width="100%" />](./3.jpg)
|
### 4.完成服务创建
|
[<img src="./4.jpg" alt="IE / Edge" width="100%" />](./4.jpg)
|
### 5.解析
|
[<img src="./5.jpg" alt="IE / Edge" width="100%" />](./5.jpg)
|
### 6.选择操作
|
[<img src="./6.png" alt="IE / Edge" width="100%" />](./6.png)
|
### 7.给接口添加数据
|
[<img src="./7.jpg" alt="IE / Edge" width="100%" />](./7.jpg)
|
### 8.状态选择
|
[<img src="./8.jpg" alt="IE / Edge" width="100%" />](./8.jpg)
|
### 9.完成接口测试
|
[<img src="./9.jpg" alt="IE / Edge" width="100%" />](./9.jpg)
|

# 本次postman创建服务教程到此结束